---
title: adamliter/teaching
layout: default
date: 2022-04-23
nav_node: teaching
---

# teaching

For past teaching and TAing that I did as an academic, see below.

I also enjoy sharing the knowledge I've learned when it comes to computers and
all things programming related. I've put together [materials for a LaTeX
workshop][latex-ling], loosely aimed at linguists, which I've taught several
times. I've also put together [materials for a `git` workshop][git-workshop],
loosely aimed at scientists who do human subjects research, which I've taught
once over the course of 7 weeks.

## Past academic teaching, TAing, and tutoring

- TA for LING 312 -- "Syntax II" (spring 2021)
- Instructor for LING 419E -- "[Topics in Syntax; Investigating Linguistic Competence: Theory and Methods][ling419e_syllabus]" (fall 2020)
- TA for LING 311 -- "Syntax I" (spring 2020)
- TA for LING 200 -- "Introductory Linguistics" (fall 2018, spring 2019)
- TA for IAH 204 -- "Asia and the World" (fall 2015, spring 2016)
- Tutor for all classes in the [Department of Philosophy at Michigan State University][msu-philosophy] (fall 2013, spring 2014)


<!-- links -->
[latex-ling]: {{ site.url }}/content/LaTeX/latex-workshop-for-linguists.pdf
[git-workshop]: https://gitlab.com/adamliter/git-workshop2018_materials
[office]: http://maps.msu.edu/interactive/index.php?location=wh
[msu-philosophy]: https://philosophy.msu.edu/
[ling419e_syllabus]: {{ site.url }}/content/syllabi/LING419E_syllabus.pdf
